package practice.jms.jms;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import practice.jms.model.Client;
import practice.jms.service.ClientService;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@Component
public class ClientListenerImpl implements ClientListener {
    private static final String CLIENT_CREATE_REQUEST = "clientCreate.in";
    private static final String CLIENT_CREATE_RESPONSE = "clientCreate.out";
    private static final String CLIENT_DELETE_REQUEST = "clientDelete.in";
    private static final String CLIENT_DELETE_RESPONSE = "clientDelete.out";
    private static final String CLIENT_BY_EMAIL_REQUEST = "clientByEmail.in";
    private static final String CLIENT_BY_EMAIL_RESPONSE = "clientByEmail.out";
    private Logger logger = LogManager.getLogger(ClientListenerImpl.class);

    public ClientListenerImpl(ClientService clientService) {
        this.clientService = clientService;
    }

    private ClientService clientService;

    @Override
    @JmsListener(destination = CLIENT_CREATE_REQUEST)
    @SendTo(CLIENT_CREATE_RESPONSE)
    public String createClient(final Message message) {
        try {
            return serialize(
                    clientService.createClient(
                            deSerialize(message)
                    )
            );
        } catch (JMSException e) {
            logger.warn("Error on creating client: ", e);
            return e.getMessage();
        }
    }

    @Override
    @JmsListener(destination = CLIENT_DELETE_REQUEST)
    @SendTo(CLIENT_DELETE_RESPONSE)
    public String deleteClient(final Message message) {
        try {
            clientService.deleteClient(deSerialize(message));
            return "client was delete successfully";
        } catch (JMSException e) {
            logger.warn("Error on deleting client: ", e);
            return e.getMessage();
        }
    }

    @Override
    @JmsListener(destination = CLIENT_BY_EMAIL_REQUEST)
    @SendTo(CLIENT_BY_EMAIL_RESPONSE)
    public String getClientByEmail(final Message message) {
        try {
        return serialize(
                clientService.getClientByEmail(
                        deSerialize(message).getEmail()
                )
        );
        } catch (JMSException e) {
            logger.warn("Error on getting client: ", e);
            return e.getMessage();
        }
    }

    private Client deSerialize(final Message message) throws JMSException {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            return new Gson().fromJson(textMessage.getText(), Client.class);
        } else {
            throw new JMSException("Invalid message");
        }
    }

    private String serialize(Client client) {
        return new Gson().toJson(client);
    }
}
