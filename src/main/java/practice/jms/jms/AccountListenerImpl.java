package practice.jms.jms;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import practice.jms.model.Account;
import practice.jms.service.AccountService;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import java.util.List;

@Component
public class AccountListenerImpl implements AccountListener {
    private static final String ACCOUNT_CREATE_REQUEST = "accountCreate.in";
    private static final String ACCOUNT_CREATE_RESPONSE = "accountCreate.out";
    private static final String ACCOUNT_OPEN_REQUEST = "accountOpen.in";
    private static final String ACCOUNT_OPEN_RESPONSE = "accountOpen.out";
    private static final String ACCOUNT_CLOSE_REQUEST = "accountClose.in";
    private static final String ACCOUNT_CLOSE_RESPONSE = "accountClose.out";
    private static final String ACCOUNT_REPLENISH_REQUEST = "accountReplenish.in";
    private static final String ACCOUNT_REPLENISH_RESPONSE = "accountReplenish.out";
    private static final String ACCOUNT_ALL_REQUEST = "accountAll.in";
    private static final String ACCOUNT_ALL_RESPONSE = "accountAll.out";
    private Logger logger = LogManager.getLogger(AccountListenerImpl.class);
    private AccountService accountService;

    @Autowired
    public AccountListenerImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    @JmsListener(destination = ACCOUNT_CREATE_REQUEST)
    @SendTo(ACCOUNT_CREATE_RESPONSE)
    public String createAccount(final Message message) {
        try {
            return serialize(
                    accountService.createAccount(
                            deSerialize(message).getClient()
                    )
            );
        } catch (JMSException e) {
            logger.warn("Error on creating account: ", e);
            return e.getMessage();
        }
    }

    @Override
    @JmsListener(destination = ACCOUNT_CLOSE_REQUEST)
    @SendTo(ACCOUNT_CLOSE_RESPONSE)
    public String closeAccount(final Message message) {
        try {
            return serialize(
                    accountService.closeAccount(
                            deSerialize(message).getNumber()
                    )
            );
        } catch (JMSException e) {
            logger.warn("Error on closing account: ", e);
            return e.getMessage();
        }
    }

    @Override
    @JmsListener(destination = ACCOUNT_OPEN_REQUEST)
    @SendTo(ACCOUNT_OPEN_RESPONSE)
    public String openAccount(final Message message) {
        try {
            return serialize(
                    accountService.openAccount(
                            deSerialize(message).getNumber()
                    )
            );
        } catch (JMSException e) {
            logger.warn("Error on opening account: ", e);
            return e.getMessage();
        }
    }

    @Override
    @JmsListener(destination = ACCOUNT_REPLENISH_REQUEST)
    @SendTo(ACCOUNT_REPLENISH_RESPONSE)
    public String replenishAccount(final Message message) {
        try {
            Account account = deSerialize(message);
            return serialize(
                    accountService.replenishAccount(account.getNumber(), account.getSum()
                    )
            );
        } catch (JMSException e) {
            logger.warn("Error on replenishing account: ", e);
            return e.getMessage();
        }
    }

    @Override
    @JmsListener(destination = ACCOUNT_ALL_REQUEST)
    @SendTo(ACCOUNT_ALL_RESPONSE)
    public String getAllAccount(final Message message) {
        try {
            Account account = deSerialize(message);
            if (!account.getClient().getRole().equals("ADMIN")) {
                throw new IllegalAccessException("Access denied");
            }
            return new Gson().toJson(
                    accountService.getAllAccount(),
                    new TypeToken<List<Account>>(){}.getType());
        } catch (JMSException | IllegalAccessException e) {
            logger.warn("Error on getting all accounts: ", e);
            return e.getMessage();
        }
    }

    private Account deSerialize(final Message message) throws JMSException {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            return new Gson().fromJson(textMessage.getText(), Account.class);
        } else {
            throw new JMSException("Invalid message");
        }
    }

    private String serialize(Account account) {
        return new Gson().toJson(account);
    }
}
