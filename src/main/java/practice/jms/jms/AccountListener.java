package practice.jms.jms;

import javax.jms.Message;

public interface AccountListener {
    String createAccount(final Message message);

    String closeAccount(final Message message);

    String openAccount(final Message message);

    String replenishAccount(final Message message);

    String getAllAccount(final Message message);
}
