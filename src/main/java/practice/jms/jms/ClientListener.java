package practice.jms.jms;

import javax.jms.Message;

public interface ClientListener {
    String createClient(final Message message);

    String deleteClient(final Message message);

    String  getClientByEmail(final Message message);
}
