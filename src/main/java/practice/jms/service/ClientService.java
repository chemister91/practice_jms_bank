package practice.jms.service;

import practice.jms.model.Client;

public interface ClientService {
    Client createClient(Client client);

    void deleteClient(Client client);

    Client getClientByEmail(String email);
}
