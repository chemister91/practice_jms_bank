package practice.jms.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import practice.jms.model.Account;
import practice.jms.model.Client;
import practice.jms.repos.AccountRepo;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepo accountRepo;

    public AccountServiceImpl(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    @Override
    public Account createAccount(Client client) {
        Account account = new Account().setClient(client);
        return accountRepo.saveAndFlush(account);
    }

    @Override
    public Account closeAccount(Integer accountNumber) {
        Account account = accountRepo.getByNumber(accountNumber).setClosed(true);
        return accountRepo.saveAndFlush(account);
    }

    @Override
    public Account openAccount(Integer accountNumber) {
        Account account = accountRepo.getByNumber(accountNumber).setClosed(false);
        return accountRepo.saveAndFlush(account);
    }

    @Override
    public Account replenishAccount(Integer accountNumber, Double sum) {
        Account account = accountRepo.getByNumber(accountNumber).addSum(sum);
        return accountRepo.saveAndFlush(account);
    }

    @Override
    public List<Account> getAllAccount() {
        return accountRepo.findAll();
    }
}
