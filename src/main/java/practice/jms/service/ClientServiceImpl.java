package practice.jms.service;

import org.springframework.stereotype.Repository;
import practice.jms.model.Client;
import practice.jms.repos.ClientRepo;

@Repository
public class ClientServiceImpl implements ClientService {
    private final ClientRepo clientRepo;

    public ClientServiceImpl(ClientRepo clientRepo) {
        this.clientRepo = clientRepo;
    }

    @Override
    public Client createClient(Client client)  {
        return clientRepo.saveAndFlush(client);
    }

    @Override
    public void deleteClient(Client client) {
        clientRepo.delete(client);
    }

    @Override
    public Client getClientByEmail(String email) {
        return clientRepo.findClientByEmail(email);
    }
}
