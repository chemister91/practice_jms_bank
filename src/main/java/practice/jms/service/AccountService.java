package practice.jms.service;

import practice.jms.model.Account;
import practice.jms.model.Client;

import java.util.List;

public interface AccountService {
    Account createAccount(Client client);

    Account closeAccount(Integer accountNumber);

    Account openAccount(Integer accountNumber);

    Account replenishAccount(Integer accountNumber, Double sum);

    List<Account> getAllAccount();

}
