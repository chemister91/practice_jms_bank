package practice.jms.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id", unique = true)
    private Integer number;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @Column
    private Double sum = 0.0;

    @Column
    private Boolean closed = false;

    public Integer getNumber() {
        return number;
    }

    public Client getClient() {
        return client;
    }

    public Account setClient(Client client) {
        this.client = client;
        return this;
    }

    public Account addSum(Double sum) {
        this.sum += sum;
        return this;
    }

    public Account withdrawSum(Double sum) {
        this.sum -= sum;
        return this;
    }

    public Double getSum() {
        return sum;
    }

    public Account setClosed(Boolean closed) {
        this.closed = closed;
        return this;
    }
}
