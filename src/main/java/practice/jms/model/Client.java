package practice.jms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "clients")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private int id;

    @Column
    private String fio;

    @Column(unique = true)
    private String email;

    @Column(name = "pass_hash")
    private String passHash;

    @Column(columnDefinition = "default 'CLIENT'")
    private String role;

    public Client setFio(String fio) {
        this.fio = fio;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Client setEmail(String email) {
        this.email = email;
        return this;
    }

    public Client setPassHash(String passHash) {
        this.passHash = passHash;
        return this;
    }

    public String getRole() {
        return role;
    }

    public Client setRole(String role) {
        this.role = role;
        return this;
    }
}
