package practice.jms.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import practice.jms.model.Account;

@Repository
public interface AccountRepo extends JpaRepository<Account, Integer>{
    Account getByNumber(@Param("account_id") Integer number);
}
