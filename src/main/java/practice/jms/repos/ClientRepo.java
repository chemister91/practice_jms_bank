package practice.jms.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import practice.jms.model.Client;

@Repository
public interface ClientRepo extends JpaRepository<Client, Integer> {
    Client findClientByEmail(@Param("email") String email);
}
